package commonPageObjects;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

//Pico container - Dependency injector class
public class TestContext {
	
	public Map<String, String> dataManager = new HashMap<String, String>();
	public WebDriver driver;
	public String scenarioName;

	public TestContext(){
	}

}
