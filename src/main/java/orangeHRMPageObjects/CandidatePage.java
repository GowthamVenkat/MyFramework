package orangeHRMPageObjects;

import commonPageObjects.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CandidatePage extends BasePage {

    public CandidatePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//nav[@aria-label='Topbar Menu']/ul/li[1]") private WebElement txtCandidateTitle;
    @FindBy(xpath = "//a[@id='menu_recruitment_viewRecruitmentModule']") private WebElement txtPassword;

    /*
     * Verify Candidate Page title
     */
    public boolean verifyCandidatesTitle() {
        boolean flag = false;
        wait.until(ExpectedConditions.visibilityOf(txtCandidateTitle));
        String actualTitle= txtCandidateTitle.getText();
        flag = CommonFunctions.ValidateFieldValue("Candidates Title Validation", actualTitle, "Candidates");
        return flag;
    }
}
