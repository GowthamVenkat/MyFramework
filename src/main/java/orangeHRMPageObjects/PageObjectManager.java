package orangeHRMPageObjects;

import org.openqa.selenium.WebDriver;

/**
 * Page Object Manager(single instance - SingleTon Pattern) - To avoid creating page object multiple times.
 * The duty of the Page Object Manager is to create the page's object and also to make sure that the same object should not be created again and
 */
public class PageObjectManager {
    private static CandidatePage candidatePage;
    private static DashBoardPage dashboardPage;
    private static LoginPage loginPage;

    private PageObjectManager(){

    }

    /**
     * If the object is not initialized in exist, It will create a new object at once.
     * @param driver
     * @return CandidatePage Object
     */
    public static CandidatePage getCandidatePage(WebDriver driver){
        return candidatePage==null ? new CandidatePage(driver) : candidatePage;
    }

    public static DashBoardPage getDashboardPage(WebDriver driver){
        return dashboardPage==null ? new DashBoardPage(driver) : dashboardPage;
    }

    public static LoginPage getLoginPage(WebDriver driver){
        return loginPage==null ? new LoginPage(driver) : loginPage;
    }
}
