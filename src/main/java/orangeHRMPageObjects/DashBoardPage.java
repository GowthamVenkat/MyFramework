package orangeHRMPageObjects;

import commonPageObjects.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DashBoardPage extends BasePage{
    public DashBoardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//div[@class='oxd-topbar-header-title']//h6") private WebElement txtDashBoardTitle;
    @FindBy(xpath = "//span[text()='Recruitment']") private WebElement lnkRecruitmentMenu;
    @FindBy(xpath = "//span[@class='oxd-userdropdown-tab']/i") private WebElement dropMenu;
    @FindBy(xpath = "(//a[@class='oxd-userdropdown-link'])[4]") private WebElement btnlogout;


    /*
     * Verify Dash board title once login
     */
    public boolean verifyDashBoardTitle() {
        boolean flag = false;
        wait.until(ExpectedConditions.visibilityOf(txtDashBoardTitle));
        String actualTitle= txtDashBoardTitle.getText();
        flag = CommonFunctions.ValidateFieldValue("DashBoard Title Validation", actualTitle, "Dashboard");
        return flag;
    }

    /*
     * Click Recruitment menu
     */
    public void clickRecruitmentMenu() {
        wait.until(ExpectedConditions.visibilityOf(lnkRecruitmentMenu));
        lnkRecruitmentMenu.click();
    }

    /*
     * Click drop menu
     */
    public void clickDropMenu() {
        wait.until(ExpectedConditions.visibilityOf(dropMenu));
        dropMenu.click();
    }

    /*
     * Click logout
     */
    public void clickLogout() {
        wait.until(ExpectedConditions.visibilityOf(btnlogout));
        btnlogout.click();
    }
}
