package orangeHRMPageObjects;

import commonPageObjects.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

public class LoginPage extends BasePage{
	@FindBy(xpath = "//input[@name='username']") private WebElement txtUserName;
	@FindBy(xpath = "//input[@name='password']") private WebElement txtPassword;
	@FindBy(xpath = "//button[@type='submit']") private WebElement btnLogin;
	@FindBy(xpath = "//div[@class='orangehrm-login-error']//p[1]") private WebElement txtUserNameCredentials;
	@FindBy(xpath = "//div[@class='orangehrm-login-error']//p[2]") private WebElement txtPasswordCredentials;

WebDriver driver;

	public LoginPage(WebDriver driver) {
        super(driver);
		this.driver = driver;
		PageFactory.initElements(driver,this);
	}

	public void enterUsernamePassword(String userName, String password) {
		wait.until(ExpectedConditions.visibilityOf(txtUserName));
		txtUserName.sendKeys(userName);
		txtPassword.sendKeys(password);
		btnLogin.click();
	}
	
	public Map<String,String> setUsernamePassword(){

		Map<String, String> mapData = new HashMap<String, String>();
		wait.until(ExpectedConditions.visibilityOf(txtUserNameCredentials));
		String userNameCredentials = txtUserNameCredentials.getText();
		String userCredentials = userNameCredentials.replace("   ", " ").trim();
		String name[] = userCredentials.split(":");
		mapData.put("Username", name[1].replace(" ", ""));

		wait.until(ExpectedConditions.visibilityOf(txtPasswordCredentials));
		String passwordCredentials = txtPasswordCredentials.getText();
		String passCredentials = passwordCredentials.replace("   ", " ").trim();
		String password[] = passCredentials.split(":");
		mapData.put("Password", password[1].replace(" ", ""));
		
		return mapData;
	}

	public boolean verifyApplicationLandedOnLoginPage() {
		boolean flag = false;
		flag = getCurrentUrl().contains("login");
		return flag;
	}
}
