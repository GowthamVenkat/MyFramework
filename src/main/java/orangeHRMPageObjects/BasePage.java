package orangeHRMPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BasePage {

    public WebDriver driver;
    public WebDriverWait wait;

    private static final Properties properties;

    static {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("src/main/resources/config/config.properties"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public BasePage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver,this);
    }

    public void launchApplication() {
        String env = properties.getProperty("ENV");
        String url;
        switch (env) {
            case "DEV": {
                url = properties.getProperty("DEV");
                break;
            }
            case "STAGE":{
                url = properties.getProperty("STAGE");
                break;
            }
            default:
                throw new IllegalStateException("INVALID ENVIRONMENT: " + env);
        }
        driver.get(url);
        driver.manage().window().maximize();
    }

    public String getCurrentUrl(){
        return driver.getCurrentUrl();
    }
}
