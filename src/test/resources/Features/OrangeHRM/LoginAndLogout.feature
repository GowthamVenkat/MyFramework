@OrangeHRMValidateLoginAndLogout
Feature: Validate Login and Logout
  	  
  @OrangeHRMValidateLogout
  Scenario: validate user able to sign in successfully
	  Given user launches the application
	  When the HR is logged in
	  Then verify HR is successfully login to dashboard
	  When user click on logout option
	  Then user verify application is land on Login page


	@FailureCase
	Scenario: validate user able to sign in successfully
		Given user launches the application
		When the HR is logged in
		Then user fail the assertion to check Retry Analyzer