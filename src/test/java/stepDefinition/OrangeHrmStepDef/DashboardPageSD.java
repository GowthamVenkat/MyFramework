package stepDefinition.OrangeHrmStepDef;


import io.cucumber.java.en.When;
import orangeHRMPageObjects.CandidatePage;
import orangeHRMPageObjects.DashBoardPage;
import orangeHRMPageObjects.PageObjectManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import commonPageObjects.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class DashboardPageSD {

	DashBoardPage dashBoardPage;
	CandidatePage candidatePage;
	public TestContext context;

	public DashboardPageSD(TestContext context){
		this.context = context;

		//invoking page objects from PageObjectManager
		dashBoardPage = PageObjectManager.getDashboardPage(context.driver);
		candidatePage = PageObjectManager.getCandidatePage(context.driver);
	}


	@Then("verify HR is successfully login to dashboard")
	public void verify_HR_is_successfully_login_to_dashboard() {
		Assert.assertTrue(dashBoardPage.verifyDashBoardTitle(), "Successfully login to dashboard");
	}
	
	@And("click Recruitment menu to navigate Recruitment screen")
	public void click_Recruitment_menu_to_navigate_Recruitment_screen() {
		dashBoardPage.clickRecruitmentMenu();
		Assert.assertTrue(candidatePage.verifyCandidatesTitle(), "Successfully navigate to Candidate Page");
	}

	@When("user click on logout option")
	public void user_click_on_logout_option() {
		dashBoardPage.clickDropMenu();
		dashBoardPage.clickLogout();
	}

	@Then("user fail the assertion to check Retry Analyzer")
	public void user_fail_the_assertion_to_check_Retry_Analyzer() {
		Assert.assertFalse(true, "Successfully login to dashboard");
	}
}
