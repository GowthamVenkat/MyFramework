package stepDefinition.OrangeHrmStepDef;


import commonPageObjects.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import orangeHRMPageObjects.LoginPage;
import orangeHRMPageObjects.PageObjectManager;
import org.testng.Assert;


public class LoginPageSD {
    public LoginPage loginPage;
	public TestContext context;
	
	public LoginPageSD(TestContext context) {
		this.context = context;

		//invoking loginPage object from PageObjectManager
		loginPage = PageObjectManager.getLoginPage(context.driver);
    }

	@Given("user launches the application")
	public void user_launches_the_application() {
		loginPage.launchApplication();
	}

	@When("the HR is logged in")
	public void the_HR_is_logged_in() {
		context.dataManager = loginPage.setUsernamePassword();
		String userName = context.dataManager.get("Username");
		String password = context.dataManager.get("Password");
		loginPage.enterUsernamePassword(userName, password);
	}

	@When("user verify application is land on Login page")
	public void user_verify_application_is_land_on_Login_page() {
		Assert.assertTrue(loginPage.verifyApplicationLandedOnLoginPage(), "Successfully logged out and landed on Login");
	}
}
