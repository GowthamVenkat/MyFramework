package stepDefinition.CommonStepDef;

import commonPageObjects.CommonFunctions;
import commonPageObjects.DriverFactory;
import commonPageObjects.TestContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;

public class Hooks {

	private WebDriver driver;
	private final TestContext context;

	//TestContext is Dependency Injector class
	public Hooks(TestContext context){
		this.context = context;
	}

	@Before
	public void before(Scenario scenario){
		context.scenarioName = scenario.getName();
		System.out.println("The Scenario Name is "+ context.scenarioName);
		System.out.println("Before: THREAD ID : " + Thread.currentThread().getId() + "," +
				"SCENARIO NAME: " + scenario.getName());
		driver = DriverFactory.initializeDriver(System.getProperty("browser", "chrome"));
		context.driver = driver;
	}



	@After
	public void tearDown(Scenario scenario) {
		System.out.println("AFTER: THREAD ID : " + Thread.currentThread().getId() + "," +
				"SCENARIO NAME: " + scenario.getName());
		try {
			String screenshotName = scenario.getName().replaceAll(" ", "_");
			if(scenario.isFailed())
				scenario.log("Scenario got Failed");
			else
				scenario.log("Scenario got Successful");
			CommonFunctions.attachScreenshot(driver,scenario, screenshotName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.quit();
	}
}
