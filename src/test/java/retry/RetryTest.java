package retry;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryTest implements IRetryAnalyzer {
    private int retry = 0;
    private static final int maxCount= 2;

    @Override
    public boolean retry(ITestResult result) {
        if(retry<maxCount){
            retry++;
            return true;
        }
        return false;
    }
}
