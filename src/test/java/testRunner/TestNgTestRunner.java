package testRunner;

import org.testng.annotations.DataProvider;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.Test;


@CucumberOptions(
		plugin= {"html:target/cucumber/cucumber.html"},
		features ="src/test/resources/Features/OrangeHRM",
		glue= {"stepDefinition"},
		monochrome = true)

@Test
public class TestNgTestRunner extends AbstractTestNGCucumberTests {
	
	@Override
	@DataProvider(parallel=true)
	public Object[][] scenarios()
	{
		return super.scenarios();
	}
}
